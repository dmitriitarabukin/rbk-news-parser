<?php
declare(strict_types=1);

namespace App\Parsers;


use DOMNode;
use Goutte\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;

class RBKNewsParser
{
    private $urlFormat = 'https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews/lastDate/%d/limit/%s';

    private $newsResources = ['www.rbc.ru', 'sportrbc.ru', 'wwww.autonews.ru', 'quote.rbc.ru', 'pro.rbc.ru', 'plus.rbc.ru', 'trends.rbc.ru'];

    /** @var Client */
    private $client;

    /** @var Collection */
    private $parsed;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->parsed = new Collection();
    }

    /**
     * Parses news and returns collection.
     *
     * @param int $number
     * @return Collection
     */
    public function parseNews(int $number = 15): Collection
    {
        $url = \sprintf($this->urlFormat, Carbon::now()->timestamp, $number);

        $this->client->request('GET', $url);

        $responseJson = $this->client->getResponse()->getContent();

        $this->parseLinks($responseJson)->each(function(DOMNode $link) {
            $url = $link->getAttribute('href');
            $host = \parse_url($url, PHP_URL_HOST);

            if (\in_array($host, $this->newsResources) === true) {
                $this->parsed->push($this->parseNewsContent($link));
            } else {
                $this->parsed->push($this->parsePromoContent($link));
            }
        });

        return $this->parsed;
    }

    /**
     * Parses news links from JSON response.
     *
     * @param string $responseJson
     * @return Collection
     */
    private function parseLinks(string $responseJson): Collection
    {
        $links = (new Collection(\json_decode($responseJson, true)['items']))
            ->pluck('html')
            ->join(' ');

        $dom = new \DOMDocument();
        $dom->loadHTML(\mb_convert_encoding($links, 'HTML-ENTITIES', 'UTF-8'));

        return new Collection($dom->getElementsByTagName('a'));
    }

    /**
     * Parses news material.
     *
     * @param DOMNode $link
     * @return array
     */
    private function parseNewsContent(DOMNode $link): array
    {
        $crawler = $this->client->request('GET', $link->getAttribute('href'));

        $titleCrawler = $crawler->filter('.js-rbcslider-slide')->first();
        try {
            $title = $titleCrawler->filter('.article__header__title-in')->text();
        } catch (\InvalidArgumentException|\RuntimeException $exception) {
            $title = $titleCrawler->filter('.js-slide-title')->text();
        }

        try {
            $imageName = $this->parseImage($crawler);
        } catch (\InvalidArgumentException|\RuntimeException $exception) {
            Log::notice('There is no image for this news: ' . $title);
        }

        $paragraphs = (new Collection($crawler->filter('.js-rbcslider-slide')->first()->filter('.article__text p')->each(function($node) {
            return $node->text();
        })))->join(' ');

        Log::info('Parsed news content: ' . $title);

        return [
            'title' => $title,
            'text' => $paragraphs,
            'image' => $imageName ?? null,
        ];
    }

    /**
     * Parses promo/advertising/partner material. Only parses title due to various styles of pages.
     *
     * @param DOMNode $link
     * @return array
     */
    private function parsePromoContent(DOMNode $link): array
    {
        $title = $link->childNodes->item(1)->textContent;

        Log::info('Parsed promo content: ' . $title);

        return [
            'title' => \trim($title),
            'text' => null,
            'image' => null,
        ];
    }

    /**
     * Parses image and stores it to local storage.
     *
     * @param Crawler $crawler
     * @return string
     */
    private function parseImage(Crawler $crawler): string
    {
        $image = $crawler->filter('.js-rbcslider-slide ')->first()->filter('.article__main-image__image')->image();
        $imageUri = $image->getUri();
        $imageName = uniqid() . '.' . File::extension($imageUri);

        $this->client->request('GET', $imageUri);

        Storage::put('news/' . $imageName, $this->client->getResponse()->getContent());

        return $imageName;
    }
}
