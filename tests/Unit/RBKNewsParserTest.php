<?php

namespace Tests\Unit;

use App\Parsers\RBKNewsParser;
use Goutte\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;
use Tests\TestCase;

class RBKNewsParserTest extends TestCase
{
    use RefreshDatabase;

    /** @var RBKNewsParser */
    private $parser;

    /** @test */
    public function should_return_collection()
    {
        $this->makeParser();
        $this->mockStorage();
        $this->mockLogger();

        $parsed = $this->parser->parseNews();

        $this->assertInstanceOf(Collection::class, $parsed);
    }

    /** @test */
    public function should_return_correct_number_of_news()
    {
        $this->makeParser();
        $this->mockStorage();
        $this->mockLogger();

        $parsed = $this->parser->parseNews();

        $this->assertCount(15, $parsed);
    }

    /** @test */
    public function should_put_correct_number_of_images_in_storage()
    {
        $this->makeParser();
        $this->mockLogger();
        Storage::fake();

        $this->parser->parseNews();

        $this->assertCount(11, Storage::allFiles('news'));
    }

    private function mockStorage(): void
    {
        Storage::shouldReceive('put')->times(11);
    }

    private function mockClient()
    {
        $responseJson = Storage::disk('local')->get('artifacts/links_response.json');
        $newsResponseHtml = Storage::disk('local')->get('artifacts/news_response_with_image.html');

        $mockedCrawler = $this->getMockBuilder(Crawler::class)
            ->setConstructorArgs([$newsResponseHtml])
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->onlyMethods([])
            ->getMock();

        $mockedClient = $this->mock(Client::class);
        $mockedClient
            ->shouldReceive('request')
            ->once()
            ->andReturn(true);
        $mockedClient
            ->shouldReceive('request')
            ->times(22)
            ->andReturn($mockedCrawler);
        $mockedClient
            ->shouldReceive('getResponse->getContent')
            ->times(12)
            ->andReturn($responseJson);

        return $mockedClient;
    }

    private function mockLogger()
    {
        Log::shouldReceive('info')->times(15);
    }

    private function makeParser(): void
    {
        $this->parser = App::make(RBKNewsParser::class, [$this->mockClient()]);
    }
}
