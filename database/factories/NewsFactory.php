<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\News;
use Faker\Generator as Faker;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

$factory->define(News::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'text' => \implode('' , $faker->paragraphs),
        'image' => Storage::disk('public')->putFile('news', new File($faker->image(storage_path('tmp')))),
    ];
});
