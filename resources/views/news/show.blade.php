@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <article>
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">{{ $news->title }}</h2>
                        @if (is_null($news->image) === false)
                            <img class="card-img-top" src="{{ asset('storage/news/' . $news->image) }}" alt="">
                        @endif
                        @if (is_null($news->text) === false)
                            <p class="card-text mt-3">{{ $news->text }}</p>
                        @endif
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
@endsection
