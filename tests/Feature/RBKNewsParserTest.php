<?php

namespace Tests\Feature;

use App\News;
use App\Parsers\RBKNewsParser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class RBKNewsParserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Real parsing test.
     *
     * @test
     */
    public function should_successfully_parse_and_insert_news_to_db()
    {
        $this->markTestSkipped('Test performs real query and should be run manually.');

        Storage::fake();

        $parser = App::make(RBKNewsParser::class);

        $parsed = $parser->parseNews()
            ->mapInto(News::class)
            ->each
            ->fill([
                'created_at' => Carbon::now(), // add timestamps manually,
                'updated_at' => Carbon::now(), // because bulk insert doesn't update timestamps
            ]);

        News::insert($parsed->toArray());

        $parsed->each(function(News $news) {
            $this->assertDatabaseHas('news', $news->toArray());
        });
        $this->assertCount(15, $parsed);
    }
}
