<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $newsList = News::all();

        return view('home', compact('newsList'));
    }
}
