@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($newsList as $news)
            <div class="col-lg-4 col-md-6 col-12 d-flex align-items-stretch mb-4">
                <div class="card">
                    <div class="card-body d-flex flex-column">
                        <h4 class="card-title">{{ $news->title }}</h4>
                        @if (is_null($news->text) === false)
                            <p class="card-text">{{ $news->short_text }}</p>
                        @endif
                        <a href="{{ route('news.show', $news->id) }}" class="btn btn-primary mt-auto">Подробнее</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
