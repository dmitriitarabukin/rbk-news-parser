<?php

namespace Tests\Feature;

use App\News;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ReadNewsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
    }

    /** @test */
    public function should_show_all_news_on_home_page()
    {
        $news = factory(News::class, 15)->create();

        $response = $this->get(route('home'));

        $news->pluck('title')->each(function(string $title) use ($response) {
            $response->assertSee($title);
        });
    }

    /** @test */
    public function should_show_a_single_news_on_news_page()
    {
        $news = factory(News::class)->create();

        $this
            ->get(route('news.show', $news->id))
            ->assertSee($news->title);
    }
}
