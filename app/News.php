<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class News extends Model
{
    protected $guarded = [];

    public function getShortTextAttribute(): string
    {
        return Str::limit($this->text, 200, '(...)');
    }
}
