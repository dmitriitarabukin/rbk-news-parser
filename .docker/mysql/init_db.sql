-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: rbk_news
-- ------------------------------------------------------
-- Server version	5.7.31-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (9,'2014_10_12_000000_create_users_table',1),(10,'2014_10_12_100000_create_password_resets_table',1),(11,'2019_08_19_000000_create_failed_jobs_table',1),(12,'2020_10_26_200303_create_news_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'2020-10-28 16:14:46','2020-10-28 16:14:46','Жизненный цикл атомного ледокола от рождения до старости',NULL,NULL),(2,'2020-10-28 16:14:47','2020-10-28 16:14:47','Байден проголосовал на выборах президента США','Кандидат в президенты США от Демократической партии Джо Байден проголосовал досрочно в штате Делавэр. Об этом сообщил телеканал CNBC. Байден проголосовал вместе со своей женой Джилл. Перед тем как проголосовать, бывший вице-президент США посетил публичные слушания о здравоохранении. Выборы президента США пройдут 3 ноября. Основными кандидатами являются Байден и действующий президент, кандидат от Республиканской партии Дональд Трамп. Трамп проголосовал досрочно 24 октября на избирательном участке в здании библиотеки в городе Уэст-Палм-Бич (штат Флорида). ','5f99c32701e90.jpg'),(3,'2020-10-28 16:14:47','2020-10-28 16:14:47','Московское «Динамо» вырвало победу у «Авангарда» в КХЛ','Омский «Авангард» в матче регулярного чемпионата Континентальной хоккейной лиги (КХЛ) проиграл московскому «Динамо» со счетом 2:3. Встреча прошла на «ВТБ Арене имени Аркадия Чернышева». Динамовцы вышли вперед на 14-й минуте. После передачи Вадима Шипачева шайбу забросил Дмитрий Кагарлицкий. После этого дважды подряд отличились хоккеисты «Авангарда». Дубль в ворота голкипера хозяев Ивана Бочарова оформил Сергей Толчинский. За 43 секунды до конца третьего периода шведский нападающий «Динамо» Оскар Линдберг перевел встречу в овертайм. На третьей минуте дополнительного периода победу столичному клубу принес точный бросок Даниила Тарасова.  «Динамо» набрало 22 очка, поднявшись на шестое место в турнирной таблице Западной конференции КХЛ. «Авангард» с 29 баллами идет вторым в Восточной конференции.','5f99c327d66d2.jpg'),(4,'2020-10-28 16:14:48','2020-10-28 16:14:48','Глава Минобрнауки назвал число полностью перешедших на удаленку вузов','По состоянию на 28 октября 152 вуза полностью перешли на удаленный формат обучения, сообщил министр образования и науки Валерий Фальков в эфире телеканала «Россия 1». «Если брать в целом, то у нас 1278 вузов и филиалов, из них 724 головных вуза, остальные — филиалы. На сегодняшний день 152 вуза полностью перешли на удаленный формат обучения студентов», — рассказал он.  При этом, по его словам, почти все вузы работают в смешанном формате, то есть часть занятий проходит очно и часть — удаленно. «Есть вузы, практически все, где формат обучения смешанный, когда студенты отчасти посещают занятия, то есть ходят в университет, может быть это совсем немного, но тем не менее, лабораторные занятия по естественно-научным дисциплинам, по физике, биологии, химии, с соблюдением особых мер предосторожности», — подчеркнул Фальков. Материал дополняется.','5f99c3280af23.jpg'),(5,'2020-10-28 16:14:48','2020-10-28 16:14:48','5 автомобильных гаджетов, которые помогут водителю зимой',NULL,NULL),(6,'2020-10-28 16:14:49','2020-10-28 16:14:49','Сеть книжных магазинов «Республика» собралась подать на банкротство','Сеть книжных магазинов «Республика» планирует подать заявление о банкротстве. Об этом сообщает ТАСС со ссылкой на пресс-службу компании. «Денежный поток компании в условиях пандемии уже не позволит исполнить обязательства и погасить задолженность перед кредиторами. Мы не видим других выходов из сложившейся ситуации и намерены подать заявление о банкротстве ООО «Республика», — говорится в сообщении.  В пресс-службе рассказали, что после снятия ограничений руководство решило, что объемы продаж возвращаются в норму, однако в конечном счете трафик стабилизировался на уровне 60% от показателей за тот же период в 2019 году. После объявления новых ограничительных мер он начал снижаться и уже не поднимался выше 50%. При этом в компании заявили, что пока не планируют закрывать магазины. Они продолжат работу в обычном режиме. РБК направил запрос основателю и владельцу сети «Республика» Вадиму Дымову.  Проблемы из-за введения ограничительных мер для борьбы с заболеваемостью СOVID-19 у «Республики» начались еще весной 2020 года. Тогда компании пришлось закрыть все торговые точки в Санкт-Петербурге. После этого генеральный директор сети Анатолий Шутка сообщил, что планирует открыть в городе новые магазины после снятия ограничений, но этого так и не произошло. Сейчас в России открыты 12 офлайн-магазинов «Республика», все они находятся в Москве. Ранее ИТ-компания «Эвотор» провела исследование, согласно которому книжные магазины в России потеряли порядка 22% прибыли в сравнении с 2019 годом. При этом самые большие потери пришлись на столичный книжный бизнес, там компании потеряли 32% выручки.','5f99c328dee4f.jpg'),(7,'2020-10-28 16:14:50','2020-10-28 16:14:50','«Автомобилист» прервал девятиматчевую победную серию «Локомотива» в КХЛ','Екатеринбургский «Автомобилист» в домашнем матче регулярного чемпионата Континентальной хоккейной лиги (КХЛ) победил ярославский «Локомотив». Встреча завершилась со счетом 1:0 в пользу хозяев. Единственную шайбу на первой минуте овертайма забросил немецкий нападающий «Автомобилиста» Брукс Мэйсек. Ему ассистировали канадский защитник Чарльз Дженовей и российский нападающий Алексей Макеев. Чешский вратарь «Автомобилиста» Якуб Коварж отразил все 35 бросков соперников. Всего в рамках КХЛ он провел 40-й «сухой» матч. «Локомотив» потерпел первое поражение с 3 октября, когда команда уступила «Йокериту» (2:3). С тех пор ярославский клуб одержал девять побед подряд — над «Витязем» (5:0), «Спартаком» (2:1 в овертайме и 1:0 по буллитам), московским «Динамо» (3:2 в овертайме), «Йокеритом» (4:2), рижским «Динамо» (3:2 по буллитам), минским «Динамо» (1:0), «Нефтехимиком» (5:1) и «Салаватом Юлаевым» (5:0). «Автомобилист» набрал 31 очко, оставшись на третьем месте в турнирной таблице Восточной конференции КХЛ. «Локомотив» с 30 баллами идет третьим в Западной конференции.','5f99c329d8456.jpg'),(8,'2020-10-28 16:14:50','2020-10-28 16:14:50','Какие карантинные меры вернули в российские регионы. Главное','27 октября Роспотребнадзор выпустил постановление об ограничениях на всей территории России:  Вторая мера носит рекомендательный характер. Именно на это сослались власти Татарстана, отказавшись ограничивать работу общепита. В Москве также решили не закрывать ночные заведения, пояснив, что уже введенного цифрового контроля их посещаемости достаточно. В Москве и регионах действуют и другие меры сдерживания инфекции. Самоизоляция для пожилых Риск тяжелого течения болезни выше у людей старше 65 лет и имеющих хронические заболевания. Поэтому власти многих регионов в числе первых мер вернули рекомендации по самоизоляции для этих граждан. К таким мерам вернулись:  Переход на удаленную работу Ряд регионов рекомендовали или потребовали перевести часть сотрудников на удаленную работу. В приоритете — сотрудники старшего возраста и имеющие хронические заболевания. Исключение составляют работники, чье присутствие на рабочем месте критически важно. Такие меры ввели:  Ограничения на проведение массовых мероприятий Во многих регионах ограничили проведение массовых мероприятий. Есть требования и к мероприятиям, которые организуют сами граждане (например, семейные торжества). Чаще всего запрет или ограничение числа участников все же касаются общественных пространств и событий.  Ограничения работы общепита и развлекательных заведений После жестких ограничений первой волны многие регионы ищут «гибридные» способы ограничений для общепита и сферы услуг, которые позволили бы не закрывать заведения. Другие ограничения Некоторые регионы объявили досрочные каникулы для школьников. Так поступили, к примеру, власти Нижегородской, Вологодской, Курганской и Сахалинской областей, а также Башкирии и Удмуртии. В Москве после каникул в школы решили вернуть только младшеклассников. Удлиненные каникулы для детей ввели Санкт-Петербург, Брянская, Новосибирская, Кировская, Тульская и Курская области и ЯНАО.  В Забайкальском крае все школы переводятся на дистанционную форму обучения. Отдать предпочтение обучению на удаленке решили также Иркутская и Кемеровская области. В Курганской области детям и подросткам запретили находиться в общественных местах без сопровождения взрослых. В Ростовской и Тюменской областях временно приостановили оказание плановой медпомощи. На въезде в Орловскую область снова заработали учетно-заградительные посты. В Якутии для вахтовиков ввели обязательную обсервацию и тестирование на наличие вируса. В Ямало-Ненецком автономном округе органы ЗАГС начнут принимать людей только по предварительной записи.','5f99c32a34870.jpg'),(9,'2020-10-28 16:14:51','2020-10-28 16:14:51','Власти разрешили МГТУ им. Баумана возобновить очное обучение','В ответ на претензии властей руководство МГТУ им. Баумана предоставило документы, доказывающие, что университет выполняет предписания, сообщило московское управление Роспотребнадзора. Поэтому управление «считает возможным» возобновить очное обучение студентов с 29 октября, говорится в сообщении.  Однако очная учеба возможно только при том условии, что администрация вуза обеспечит контроль за соблюдением всех мер, действующих из-за пандемии коронавируса. «В университет пришло предписание, что мы все выполнили, и нет оснований для того, чтобы нам запрещать очную работу. Соответственно, с завтрашнего дня мы выходим учиться очно. Все те корпуса, которые были на изоляции, возвращаются в очный режим работы», — рассказали РБК пресс-службе МГТУ им. Н.Э. Баумана. Там добавили, что у тех студентов, кто заболел COVID-19 или находился в контактной группе с зараженными, продолжится дистанционный формат обучения. Материал дополняется. ','5f99c32aa7899.jpg'),(10,'2020-10-28 16:14:51','2020-10-28 16:14:51','Кто выиграл, а кто проиграл от масштабного перехода на удаленку',NULL,NULL),(11,'2020-10-28 16:14:51','2020-10-28 16:14:51','Соратник схимонаха Сергия объяснил COVID-19 его отказ ходить на допросы','Отлученный от церкви схимонах Сергий Романов не хочет ходить на допросы по уголовным делам в Следственный комитет, поскольку он опасается заразиться на них коронавирусом. Об этом заявил в видеообращении на YouTube его соратник Всеволод Могучев. «Следственный комитет теряет от COVID-19 все больше и больше сотрудников. В этот источник вирусной инфекции у схиигумена Сергия есть все основания не ходить», — сказал он, отметив, что следователи вынуждают священника лично приехать на допрос. Схиигумен Сергий сместил игуменью Среднеуральского женского монастыря и фактически захватил в нем власть в середине июня. Вскоре схиигумен записал видеообращение, где назвал коронавирус попыткой чипировать население, а также призвал к отставке патриарха Кирилла и тех архиереев, «которые нарушали церковные правила, встречались с папами римскими, с кардиналами, принимали участие в совместных молитвах». 3 июля церковный суд в Екатеринбурге лишил его сана, а перед этим запретил его в служении. Сергия признали нарушившим священническую присягу и монашеские обеты. В сентябре схиигумена отлучили от церкви.  В начале 2000-х Сергий участвовал в строительстве Среднеуральского монастыря и был его первым духовником. Жившие в обители несовершеннолетние воспитанники позже сообщали о жестоком обращении и издевательствах. В начале октября Следственный комитет завел уголовное дело по факту истязания детей на территории монастыря, а также ненадлежащего исполнения обязанностей должностными лицами органов опеки по п. «а, г» ч. 2 ст. 117 УК (истязание) и ч. 1 ст. 293 УК (халатность). Схиигумен проходит по этому делу в качестве свидетеля.  В августе стало известно о смерти в монастыре 15-летней девочки. Глава Следственного комитета Александр Бастрыкин поручил проверить сообщения о смерти. Сам схиигумен объяснил смерть девочки раком мозга. По его словам, девочка с семьей жила в монастыре семь месяцев и к ней ежедневно приезжал врач.','5f99c32b43109.jpg'),(12,'2020-10-28 16:14:52','2020-10-28 16:14:52','Германия компенсирует малому бизнесу до 75% дохода из-за ограничений','Правительство Германии намерено возместить большую часть дохода небольшим компаниям в связи с введением в стране новых ограничительных мер из-за распространения коронавирусной инфекции. Об этом в ходе пресс-конференции заявила канцлер ФРГ Ангела Меркель, передает агентство Reuters. Она пояснила, что речь идет о предприятиях, где работают до 50 сотрудников. Они получат 75% от выручки за ноябрь 2019 года. На это правительство Германии намерено выделить €10 млрд.  Ранее 28 октября с таким предложением выступил министр финансов Германии Олаф Шольц, сообщала газета Bild. По его словам, небольшие компании должны получить до 75% потерянного оборота, а более крупные — до 70%. Меркель заявила, что новые карантинные меры в Германии вступят в силу с 2 ноября. Согласно распоряжению канцлера, будут закрыты все развлекательные предприятия, а также места общественного питания. Магазины будут открыты, но с ограничениями. Кроме того, отели не смогут принимать постояльцев, которые путешествуют в туристических целях. Также гражданам будет запрещено собираться компаниями более десяти человек. По словам Меркель, новые меры продлятся в стране как минимум до декабря.  По данным Всемирной организации здравоохранения, за прошедшие сутки в Германии коронавирусом заразились около 15 тыс. человек. Это самый высокий прирост в стране за все время пандемии. Первый случай заражения COVID-19 в ФРГ зарегистрировали 28 января 2020 года. С тех пор в Германии коронавирусом заболели более 464 тыс. человек. ','5f99c32be04c0.jpg'),(13,'2020-10-28 16:14:52','2020-10-28 16:14:52','Денис Савостин — какой бизнес ни в коем случае не стоит открывать',NULL,NULL),(14,'2020-10-28 16:14:52','2020-10-28 16:14:52','СМИ узнали о временном закрытии академии «Спартака» из-за коронавируса','Академию московского «Спартака» закрыли на две недели из-за ухудшения эпидемиологической ситуации. Об этом сообщает «Чемпионат» со ссылкой на источники, знакомые с ситуацией. «Все игроки отпущены по домам. Они не будут тренироваться две недели», — заявили в академии. РБК направил запрос в ФК «Спартак». Ранее «Спартак» из-за ограничений, связанных с распространением коронавируса, закрыл две трибуны на матч 13-го тура чемпионата России против «Ростова». Встреча состоится в субботу, 31 октября. Квота болельщиков на игру, согласно распоряжению Роспотребнадзора, составит 6% от вместимости «Открытие Арены» (2730 человек). Для зрителей будут закрыты трибуны C и D. Обладателей абонементов на трибуну D пересадят на трибуну В, а держателей абонементов на трибуну С — на трибуну А. Общее количество зрителей будет в несколько раз меньше числа проданных абонементов. Зрители смогут попасть на стадион по итогам жеребьевки, которая состоится в четверг, 29 октября.  «Спартак» объявил, что обладателям абонементов, не попавшим на матч, вернут потраченные деньги. В качестве альтернативного варианта им будет предложено посещение матча против «Краснодара» в рамках второго круга чемпионата России или встречи 1/8 финала Кубка страны.','5f99c32c9a32a.jpg'),(15,'2020-10-28 16:14:54','2020-10-28 16:14:54','Минздрав объяснил приказ о высказываниях сотрудников по коронавирусу','Письмо Минздрава, в котором министерство потребовало от подведомственных учреждений согласовывать публичные комментарии по коронавирусу с пресс-службой, нацелено на то, чтобы добиться единообразия подходов к борьбе с эпидемией. Об этом заявил РБК помощник министра здравоохранения Алексей Кузнецов. Он пояснил, что после начала эпидемии в России «информационное поле стало наполняться различными домыслами, прогнозами, рекомендациями — порой диаметрально противоположными». К примеру, эксперты расходились во мнениях относительно эффективности ношения перчаток, существования действующих лекарств от COVID-19, целесообразности применения антибиотиков для профилактики осложнений при коронавирусной инфекции. «Различные организации и ассоциации стали публиковать свои методические рекомендации по лечению коронавирусной инфекции, не имеющие отношения к официальным рекомендациям Минздрава. В результате практикующие врачи оказывались в замешательстве — каким рекомендациям следовать. Граждане должны иметь доступ к достоверной информации, а не к досужим домыслам», — пояснил Кузнецов. Кузнецов отметил, что в большинстве случаев в названиях подведомственных учреждений и должностей внештатных специалистов указывается их принадлежность к Минздраву (например, главный внештатный инфекционист Минздрава). Из-за этого высказывания руководителей или сотрудников подведомственных Минздраву учреждений или главных внештатных специалистов трактуется и воспринимается как позиция самого министерства. «Именно поэтому мы обратились к руководителям наших организаций и уважаемым главным внештатным специалистам с просьбой координировать с министерством свою активность по информированию граждан о мерах по профилактике и лечению новой коронавирусной инфекции», — заключил помощник министра. Кузнецов указал на то, что министерство не намерено таким образом ограничивать для врачей возможность давать публичные комментарии, в том числе и по проблемным вопросам.  Письмо, в котором предлагалось согласовывать все комментарии с пресс-службой министерства, Минздрав разослал по подведомственным учреждениям. В министерстве указывали, что к ним не относятся, к примеру, больницы, находящиеся в ведении региональных властей.','5f99c32ce626f.jpg');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-29  5:28:23
